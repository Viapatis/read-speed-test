export default [
    {
        text: 'О проекте',
        refClick: '/project-info'
    },
    {
        text: 'Уроки',
        refClick: '/lessons'
    },
    {
        text: 'Рейтинг',
        refClick: '/rating'
    },
    {
        text: 'Отзывы',
        refClick: '/reviews'
    }
]