import React from 'react';
import './styles/Page.css';
export default function Page(props) {
    const { children, direction } = { ...props };
    return (
        <div className={`page-wrap wrap-${direction}`}>
            {children}
        </div>
    )
}