import React from 'react';
import './styles/HeaderMenu.css';
import HeaderMenuItem from './HeaderMenuItem';
export default function HeaderMenu(props) {
    const { items } = { ...props };
    return (
        <nav className='header-menu-wrap'>
            <ul className='header-menu'>
                {items.map((item, index) => {
                    return <HeaderMenuItem key={index + ''} {...item}></HeaderMenuItem>;
                })}
            </ul>
        </nav>
    )
}