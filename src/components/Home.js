import React, { useCallback } from 'react';
import Header from './Header';
import PageContent from './PageContent';
import Page from './Page';
import ContentBlock from './ContentBlock';
import ButtonSR from './ButtonSR';
import projectInfo from '../projectInfo';
export default function Home(props) {
    const { history } = { ...props };
    const startTest = useCallback(() => {
        props.history.push('/speed-read-test');
    }, []);
    return (
        <Page direction={'col'}>
            <Header projectInfo ={projectInfo} history={history} ></Header>
            <PageContent>
                <ContentBlock mainBlock={true} title={`Добро пожаловать на сайт ${projectInfo.projectName}`}>
                    <div style={{ width: '700px', height: '200px', backgroundColor: '#C4C4C4', marginBottom: '20px' }}></div>
                    <div style={{ width: '700px', height: '200px', backgroundColor: '#C4C4C4', marginBottom: '20px' }}></div>
                </ContentBlock>
                <ContentBlock title={`Популярные статьи`}>
                </ContentBlock>
                <ContentBlock title={`Измерь скорость чтения`}>
                    <span>Демо-текст </span>
                    <ButtonSR className='content-button' onClick={startTest}>Начать!</ButtonSR>
                </ContentBlock>
            </PageContent>
        </Page>
    )
}