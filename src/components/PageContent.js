import React from 'react';
import './styles/PageContent.css';
export default function PageContent(props) {
    const { className, children } = { ...props };
    const [main, ...otherChildrens] = [...children];
    return (
        <div className={`content-body-wrap${className ? ' ' + className : ''}`}>
            <div className='content-body-main'>{main}</div>
            <div className='content-body-other'>{otherChildrens}</div>
        </div>
    )
}