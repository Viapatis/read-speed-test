import React from 'react';
import './styles/ButtonSR.css';
export default function ButtonSR(props) {
    const { children, className, ...rest } = { ...props };
    return (
        <button {...rest} className={`button ${className}`}>{children}</button>
    )
}