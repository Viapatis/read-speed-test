import React from 'react';
import './styles/HeaderMenu.css';
import { Link } from 'react-router-dom';
export default function HeaderMenuItem(props) {
    const { text, className, refClick, ...rest } = { ...props };
    return (
        <li className={'header-menu-item' + (className ? ` ${className}` : '')} {...rest}>
            <Link to={refClick}>{text}</Link>
        </li>
    )
}