import React, { useCallback } from 'react';
import './styles/Header.css';
import HeaderMenu from './HeaderMenu';
import ButtonSR from './ButtonSR';
import HeaderMenuItems from './headerMenuItems'
import logo from '../logo.svg';
import { Route } from 'react-router-dom';
export default function Header(props) {
    const logInClick = useCallback(() => {
        props.history.push('/login');
    }, []);
    const registrationClick = useCallback(() => {
        props.history.push('/registration');
    }, []);
    return (
        <div className='header'>
            <div className='header-logo-block'>
                <img alt='projectName' className='header-logo-block__logo' src={logo} />
                {props.projectInfo.projectName}
            </div>
            <HeaderMenu items={HeaderMenuItems} />
            <Route>
                <ButtonSR className='header-button' onClick={logInClick}>Войти</ButtonSR>
                <ButtonSR className='header-button' onClick={registrationClick}>Зарегистрироваться</ButtonSR>
            </Route>
        </div>
    )
}