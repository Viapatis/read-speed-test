import React from 'react';
import './styles/ContentBlock.css';
export default function ContentBlock(props) {
    const { title, mainBlock, children } = { ...props };
    const header = mainBlock ? <h1 className='content-block-header content-block-header-main'>{title}</h1> :
        <h3 className='content-block-header'>{title}</h3>;
    return (
        <div className={'content-block-wrap' + `${mainBlock ? ' content-block-wrap-main' : ''}`}>
            {header}
            <div className='content-block-body'>
                {children}
            </div>
        </div>
    )
}