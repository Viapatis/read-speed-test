import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import SpeedTest from './SpeedTest'
export default function Main(props) {
    return (
        <main className='main' style={{height:'100%'}}>
            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/speed-read-test' component={SpeedTest} />
            </Switch>
        </main>
    )
}